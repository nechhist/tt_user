package telegram

import "net/http"

const (
	// TokenBotTelegram ...
	TokenBotTelegram = "184883614:AAHAthCjIqTcNPNL0TEVOXkEeDxdmANs-ac"
	// ChatIDTelegram ...
	ChatIDTelegram = "-1001060781892"
)

// SendMessage ...
func SendMessage(message string) error {
	var url = "https://api.telegram.org/bot" + TokenBotTelegram + "/sendMessage?chat_id=" + ChatIDTelegram + "&text=" + message
	_, err := http.Get(url)
	return err
}
