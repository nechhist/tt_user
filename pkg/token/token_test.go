package token

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGood(t *testing.T) {
	var text = "Hello World"
	var timeDelay int64 = 1
	var key = "test"

	str, err := Create(text, key)
	assert.Nil(t, err)

	payload, err := GetPayload(str, key, timeDelay)
	assert.Nil(t, err)

	if text != payload {
		t.Errorf("Token payload has not actual text, text: %s, payload: %s", text, payload)
	}
}

func TestBad(t *testing.T) {
	var text = "Hello World"
	var timeDelay int64 = 1
	var key = "test"

	str, err := Create(text, key)
	assert.Nil(t, err)

	// check fail text
	failStr := text + "123"
	_, err = GetPayload(failStr, key, timeDelay)
	assert.NotNil(t, err)

	// check fail key
	failKey := key + "123"
	_, err = GetPayload(str, failKey, timeDelay)
	assert.NotNil(t, err)

	// check fail timeDelay
	timeDelay = 0
	time.Sleep(time.Second)
	_, err = GetPayload(str, key, timeDelay)
	assert.NotNil(t, err)
}
