package token

import (
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

var (
	// ErrTimeOut ...
	ErrTimeOut = errors.New("token's time out")
)

// token ...
type token struct {
	payload string
	time    string
	hash    string
}

// GetPayload ...
func GetPayload(str, key string, timeDelay int64) (string, error) {
	t, err := decode(str, key)
	if err != nil {
		return "", err
	}

	// check time
	timeNow := time.Now().Unix()
	timeMin := timeNow - timeDelay
	timeMax := timeNow + timeDelay
	timeToken, _ := strconv.ParseInt(t.time, 10, 64)

	if timeToken < timeMin || timeToken > timeMax {
		return "", ErrTimeOut
	}

	return t.payload, nil
}

// Create ...
func Create(payload, key string) (string, error) {
	if len(payload) == 0 {
		return "", errors.New("token cant create, payload is empty")
	}
	if len(key) == 0 {
		return "", errors.New("token cant create, key is empty")
	}

	str := encode(payload, key)
	return str, nil
}

// decode ...
func decode(str, key string) (*token, error) {
	decoded, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return nil, fmt.Errorf("token base64 decode error: %v", err)
	}

	split := strings.Split(string(decoded), ".")
	if len(split) != 3 {
		return nil, errors.New("token string is broken")
	}

	token := &token{
		payload: split[0],
		time:    split[1],
		hash:    split[2],
	}

	// check hash
	hash := getHash(token.payload, token.time, key)
	if token.hash != hash {
		return nil, errors.New("token has incorrect hashsum, token: " + str)
	}

	return token, nil
}

// encode ...
func encode(payload, key string) string {
	now := time.Now().Unix()
	timeString := strconv.Itoa(int(now))
	hash := getHash(payload, timeString, key)

	str := payload + "." + timeString + "." + hash

	encoded := base64.StdEncoding.EncodeToString([]byte(str))
	return encoded
}

// getHash ...
func getHash(payload, time, key string) string {
	hash := sha256.Sum256([]byte(payload + time + key))
	return fmt.Sprintf("%s", hash)
}
