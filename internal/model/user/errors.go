package user

import "errors"

var (
	// ErrRecordNotFound ...
	ErrRecordNotFound = errors.New("Запись в базе данных не обнаружена.")
	// ErrUserNameNotUnique ...
	ErrUserNameNotUnique = errors.New("Имя пользователя не уникально.")
	// ErrIncorrectNameOrPassword ...
	ErrIncorrectNameOrPassword = errors.New("Неправильное имя пользователя или пароль.")
)
