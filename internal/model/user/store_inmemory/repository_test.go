package store_inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nechhist/tt_user/internal/model/user"
)

func TestRepository_Create(t *testing.T) {
	store := New()
	u := getTestUser(t)
	assert.NoError(t, store.Repository().Create(u))
	assert.NotNil(t, u.ID)
}

func TestRepository_FindById(t *testing.T) {
	store := New()
	u1 := getTestUser(t)
	err := store.Repository().Create(u1)
	assert.NoError(t, err)
	u2, err := store.Repository().FindById(u1.ID)
	assert.NoError(t, err)
	assert.NotNil(t, u2)
}

func TestRepository_FindByName(t *testing.T) {
	store := New()
	u1 := getTestUser(t)
	_, err := store.Repository().FindByName(u1.Name)
	assert.EqualError(t, err, user.ErrRecordNotFound.Error())
	err = store.Repository().Create(u1)
	assert.NoError(t, err)
	u2, err := store.Repository().FindByName(u1.Name)
	assert.NoError(t, err)
	assert.NotNil(t, u2)
}

// getTestUser ...
func getTestUser(t *testing.T) *user.User {
	t.Helper()

	return &user.User{
		Name:     "test",
		Email:    "test@test.org",
		Password: "password",
	}
}
