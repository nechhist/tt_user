package store_inmemory

import (
	"gitlab.com/nechhist/tt_user/internal/model/user"
)

// Repository ...
type Repository struct {
	users map[int]*user.User
}

// Create ...
func (r *Repository) Create(u *user.User) error {
	u.ID = len(r.users) + 1
	r.users[u.ID] = u
	return nil
}

// FindById ...
func (r *Repository) FindById(id int) (*user.User, error) {
	u, ok := r.users[id]
	if !ok {
		return nil, user.ErrRecordNotFound
	}

	return u, nil
}

// FindByName ...
func (r *Repository) FindByName(name string) (*user.User, error) {
	for _, u := range r.users {
		if u.Name == name {
			return u, nil
		}
	}

	return nil, user.ErrRecordNotFound
}

// FindByEmail ...
func (r *Repository) FindByEmail(email string) (*user.User, error) {
	for _, u := range r.users {
		if u.Email == email {
			return u, nil
		}
	}

	return nil, user.ErrRecordNotFound
}

// IsUniqueName ...
func (r *Repository) IsUniqueName(name string) error {
	for _, u := range r.users {
		if u.Name == name {
			return user.ErrUserNameNotUnique
		}
	}

	return nil
}
