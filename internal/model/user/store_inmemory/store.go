package store_inmemory

import (
	"gitlab.com/nechhist/tt_user/internal/model/user"
)

// Store ...
type Store struct {
	repository *Repository
}

// New ...
func New() *Store {
	return &Store{}
}

// Repository ...
func (s *Store) Repository() user.Repository {
	if s.repository != nil {
		return s.repository
	}

	s.repository = &Repository{
		users: make(map[int]*user.User),
	}

	return s.repository
}
