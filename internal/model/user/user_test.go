package user

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUserValidate(t *testing.T) {
	testCases := []struct {
		name    string
		getUser func() *User
		isValid bool
	}{
		{
			name: "valid",
			getUser: func() *User {
				return GetTestUser(t)
			},
			isValid: true,
		},
		{
			name: "with encrypted password",
			getUser: func() *User {
				u := GetTestUser(t)
				u.Password = ""
				u.EncryptedPassword = "encpass"
				return u
			},
			isValid: true,
		},
		// {
		// 	name: "empty email",
		// 	getUser: func() *User {
		// 		u := GetTestUser(t)
		// 		u.Email = ""

		// 		return u
		// 	},
		// 	isValid: false,
		// },
		{
			name: "invalid email",
			getUser: func() *User {
				u := GetTestUser(t)
				u.Email = "invalid"
				return u
			},
			isValid: false,
		},
		{
			name: "empty password",
			getUser: func() *User {
				u := GetTestUser(t)
				u.Password = ""

				return u
			},
			isValid: false,
		},
		{
			name: "short password",
			getUser: func() *User {
				u := GetTestUser(t)
				u.Password = "short"

				return u
			},
			isValid: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.isValid {
				assert.NoError(t, tc.getUser().Validate())
			} else {
				assert.Error(t, tc.getUser().Validate())
			}
		})
	}
}

func TestUser_BeforeCreate(t *testing.T) {
	u := GetTestUser(t)
	assert.NoError(t, u.BeforeCreate())
	assert.NotEmpty(t, u.EncryptedPassword)
}

// GetTestUser ...
func GetTestUser(t *testing.T) *User {
	t.Helper()

	return &User{
		Name:     "test",
		Email:    "test@test.org",
		Password: "password",
	}
}
