package user

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"golang.org/x/crypto/bcrypt"
)

const (
	// TYPE_BANNED ...
	TYPE_BANNED = 0
	// TYPE_ADMIN ...
	TYPE_ADMIN = 1
	// TYPE_STANDART ...
	TYPE_STANDART = 2
)

// User ...
type User struct {
	ID                int    `json:"id"`
	Name              string `json:"username"`
	Email             string `json:"email"`
	Password          string `json:"password,omitempty"`
	EncryptedPassword string `json:"-"`
	CreateAt          int    `json:"-"`
	Type              int    `json:"-"`
}

// Validate ...
func (u *User) Validate() error {
	return validation.ValidateStruct(
		u,
		validation.Field(&u.Name, validation.Required, validation.Length(4, 16)),
		//validation.Field(&u.Email, validation.Required, is.Email),
		validation.Field(&u.Email, is.Email),
		validation.Field(&u.Password, validation.By(equalFoo(u.EncryptedPassword == "")), validation.Length(6, 100)),
	)
}

// equalFoo ...
func equalFoo(cond bool) validation.RuleFunc {
	return func(value interface{}) error {
		if cond {
			return validation.Validate(value, validation.Required)
		}

		return nil
	}
}

// BeforeCreate ...
func (u *User) BeforeCreate() error {
	if len(u.Password) > 0 {
		b, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.MinCost)
		if err != nil {
			return err
		}
		u.EncryptedPassword = string(b)
	}
	u.Password = ""
	u.Type = TYPE_STANDART
	return nil
}

// ComparePassword ...
func (u *User) ComparePassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.EncryptedPassword), []byte(password)) == nil
}
