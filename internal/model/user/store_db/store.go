package store_db

import (
	"database/sql"

	"gitlab.com/nechhist/tt_user/internal/model/user"
)

// Store ...
type Store struct {
	db         *sql.DB
	repository *Repository
	// cache ...
}

// New ...
func New(db *sql.DB) *Store {
	return &Store{
		db: db,
	}
}

// Repository ...
func (s *Store) Repository() user.Repository {
	if s.repository != nil {
		return s.repository
	}

	s.repository = &Repository{
		store: s,
	}

	return s.repository
}
