package store_db

import (
	"database/sql"
	"time"

	"gitlab.com/nechhist/tt_user/internal/model/user"
)

// Repository ...
type Repository struct {
	store *Store
}

// Create ...
func (r *Repository) Create(u *user.User) error {
	stmt, err := r.store.db.Prepare("INSERT INTO users (name, email, encrypted_password, create_at) VALUES (?, ?, ?, ?)")
	if err != nil {
		return err
	}

	res, err := stmt.Exec(u.Name, u.Email, u.EncryptedPassword, time.Now().Unix())
	if err != nil {
		return err
	}

	id, _ := res.LastInsertId()
	u.ID = int(id)
	return nil
}

// FindById ...
func (r *Repository) FindById(id int) (*user.User, error) {
	u := &user.User{}
	if err := r.store.db.QueryRow(
		"SELECT id, name, email, encrypted_password FROM users WHERE id = ?",
		id,
	).Scan(
		&u.ID,
		&u.Name,
		&u.Email,
		&u.EncryptedPassword,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, user.ErrRecordNotFound
		}
		return nil, err
	}

	return u, nil
}

// FindByName ...
func (r *Repository) FindByName(name string) (*user.User, error) {
	u := &user.User{}
	if err := r.store.db.QueryRow(
		"SELECT id, name, email, encrypted_password FROM users WHERE name = ?",
		name,
	).Scan(
		&u.ID,
		&u.Name,
		&u.Email,
		&u.EncryptedPassword,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, user.ErrRecordNotFound
		}
		return nil, err
	}

	return u, nil
}

// IsUniqueName ...
func (r *Repository) IsUniqueName(name string) error {
	if _, err := r.FindByName(name); err != nil {
		return user.ErrUserNameNotUnique
	}

	return nil
}
