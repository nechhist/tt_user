package user

// Repository ...
type Repository interface {
	Create(*User) error
	FindById(int) (*User, error)
	FindByName(string) (*User, error)
	IsUniqueName(string) error
}
