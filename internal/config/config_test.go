package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheck(t *testing.T) {
	c := GetTestConfig(t)
	assert.Nil(t, c.Validate())
}

func GetTestConfig(t *testing.T) *Config {
	t.Helper()
	return &Config{
		LogLevel:   "string",
		ServerPort: "string",
		AllowedOrigins: []string{"*"},
		
		DbUser:     "string",
		DbPassword: "string",
		DbName:     "string",
		DbHost:     "string",
		DbPort:     "string",
		DbTable:    "string",

		RedisUser:     "string",
		RedisPassword: "string",
		RedisHost:     "string",
		RedisPort:     "string",

		SessionKey: "string",

		TokenKey: "string",
	}
}
