package config

import (
	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/kelseyhightower/envconfig"
)

// Config represents an application configuration.
type Config struct {

	// LogLevel ...
	LogLevel string

	// ServerPort ...
	ServerPort string

	// AllowedOrigins ...
	AllowedOrigins []string

	// DbUser ...
	DbUser string
	// DbPassword ...
	DbPassword string
	// DbName ...
	DbName string
	// DbHost ...
	DbHost string
	// DbPort ...
	DbPort string
	// DbDbTable
	DbTable string

	// MqUser ...
	RedisUser string
	// MqPassword ...
	RedisPassword string
	// MqHost ...
	RedisHost string
	// MqPort ...
	RedisPort string
	// SessionKey ...
	SessionKey string
	// TokenKey ...
	TokenKey string
}

// Load returns an application configuration.
func Load() *Config {
	c := &Config{}
	envconfig.Process("app", c)
	return c
}

// Validate validates the application configuration.
func (c Config) Validate() error {
	return validation.ValidateStruct(&c,
		// server
		validation.Field(&c.ServerPort, validation.Required),
		// AllowedOrigins
		validation.Field(&c.AllowedOrigins, validation.Required),
		// DB
		validation.Field(&c.DbName, validation.Required),
		validation.Field(&c.DbPassword, validation.Required),
		validation.Field(&c.DbPort, validation.Required),
		validation.Field(&c.DbTable, validation.Required),
		validation.Field(&c.DbHost, validation.Required),
		// Redis
		//validation.Field(&c.RedisHost, validation.Required),
		//validation.Field(&c.RedisPassword, validation.Required),
		//validation.Field(&c.RedisPort, validation.Required),
		//validation.Field(&c.RedisUser, validation.Required),
		// Session
		validation.Field(&c.SessionKey, validation.Required),
		validation.Field(&c.TokenKey, validation.Required),
	)
}
