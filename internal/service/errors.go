package service

import (
	"errors"
)

var (
	// ErrNotAuthenticated ...
	ErrNotAuthenticated = errors.New("not authenticated")
)
