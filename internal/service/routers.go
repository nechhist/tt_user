package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/nechhist/tt_user/internal/model/user"
	"gitlab.com/nechhist/tt_user/pkg/telegram"
	"gitlab.com/nechhist/tt_user/pkg/token"
)

const (
	// SessionCookieName ...
	SessionCookieName = "session_id"
	// HeaderSessionID ...
	HeaderSessionID = "x-session-id"
	// HeaderUserToken ...
	HeaderUserToken = "x-user-token"
	// HeaderUserName ...
	HeaderUserName = "x-user-name"
)

var (
	// errResponseSpam ...
	errResponseSpam = fmt.Errorf("Слишком много регистраций с данного IP адреса. Подождите %d мин.", spamDelay)
	// errContextValueUser ...
	errContextValueUser = fmt.Errorf("Context Value ctxUser failed")
)

var (
	urlApiV1 = "/api/v1"

	urlRegistration  = "/signup"
	urlAuthorization = "/signin"
	urlLogout        = "/logout"

	urlUser           = "/user"
	urlIdentification = "/id"
)

// ResponseAuth ...
type ResponseAuth struct {
	Name      string
	Token     string
	SessionID string
}

// getRouters ...
func (s *service) getRouters() *mux.Router {
	router := mux.NewRouter()

	router.Use(s.accessMiddleware)
	router.Use(s.errorMiddleware)

	// Where ORIGIN_ALLOWED is like `scheme://dns[:port]`, or `*` (insecure)
	headersOk := handlers.AllowedHeaders([]string{"content-type", "set-cookie", HeaderSessionID, HeaderUserName, HeaderUserToken})
	originsOk := handlers.AllowedOrigins(s.config.AllowedOrigins)
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	credentialsOk := handlers.AllowCredentials()
	router.Use(handlers.CORS(credentialsOk, headersOk, originsOk, methodsOk))

	router.HandleFunc("/healthcheck", s.handleHealthcheck()).Methods("GET")

	apiV1 := router.PathPrefix(urlApiV1).Subrouter()

	// example: curl -i -X POST -d "{\"username\":\"admin\",\"email\":\"email@email.com\",\"password\":\"qwerty\"}" http://localhost:15800/api/v1/signup
	apiV1.HandleFunc(urlRegistration, s.handleRegistration()).Methods("POST")
	// example: curl -i -X POST -d "{\"username\":\"admin\",\"email\":\"email@email.com\",\"password\":\"qwerty\"}" http://localhost:15800/api/v1/signin
	apiV1.HandleFunc(urlAuthorization, s.handleAuthorization()).Methods("POST")
	// example: curl -i -X GET --cookie "session_id=" http://localhost:15800/api/v1/logout
	apiV1.HandleFunc(urlLogout, s.handleLogout()).Methods("GET")

	// DUMMY
	apiV1.HandleFunc(urlRegistration, serveDummy).Methods("OPTIONS")
	apiV1.HandleFunc(urlAuthorization, serveDummy).Methods("OPTIONS")

	user := apiV1.PathPrefix(urlUser).Subrouter()
	user.Use(s.authenticationMiddleware)
	// example: curl -i -X GET --cookie "session_id=MTU4MjgxMTgyNXxEdi1CQkFFQ180SUFBUkFCRUFBQUh2LUNBQUVHYzNSeWFXNW5EQWtBQjNWelpYSmZhV1FEYVc1MEJBSUFBZz09fJogZLEL6MHP_kLtZpZYMLfEvhdSAFDJaXyqhBbIpC8P" http://localhost:15800/api/v1/user/id
	user.HandleFunc(urlIdentification, s.handleIdentification()).Methods("GET")

	// user := api.PathPrefix("/user").Subrouter()
	// user.Use(s.authenticationMiddleware)
	// example: curl -i -X GET --cookie "user_session_id=1234" http://localhost:15801/api/v1/
	// user.HandleFunc("/", s.handleApiUserIndex()).Methods("GET")

	return router
}

// handleApiUserIndex ...
// func (s *service) handleApiUserIndex() http.HandlerFunc {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		s.response(w, r, http.StatusOK, r.Context().Value(ctxUser))
// 	}
// }

// serveDummy
func serveDummy(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello!")
}

// handleIdentification ...
func (s *service) handleIdentification() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		u, ok := r.Context().Value(ctxUser).(*user.User)
		if !ok {
			s.logger.Error("handleIdentification error:", http.StatusInternalServerError, errContextValueUser)
			s.response(w, r, http.StatusInternalServerError, errContextValueUser)
			return
		}

		//s.logger.Debug("handleIdentification user:", u)
		token, err := token.Create(u.Name, s.config.TokenKey)
		if err != nil {
			s.logger.Error("handleIdentification error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}
		w.Header().Add(HeaderSessionID, "")
		w.Header().Add(HeaderUserName, u.Name)
		w.Header().Add(HeaderUserToken, token)
		s.response(w, r, http.StatusOK, ResponseAuth{u.Name, token, ""})
	}
}

// handleLogout ...
func (s *service) handleLogout() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		session, err := s.sessionStore.Get(r, SessionCookieName)
		if err != nil {
			s.logger.Info("handleLogout error:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		session.Options.MaxAge = -1
		if err := s.sessionStore.Save(r, w, session); err != nil {
			s.logger.Error("handleLogout error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}
		w.Header().Add(HeaderSessionID, "")
		w.Header().Add(HeaderUserName, "")
		w.Header().Add(HeaderUserToken, "")
		s.response(w, r, http.StatusOK, nil)
	}
}

type requestRegistration struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// handleRegistration ...
func (s *service) handleRegistration() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := &requestRegistration{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.logger.Debug("handleRegistration error:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err)
			return
		}

		u := &user.User{
			Name:     req.Username,
			Email:    req.Email,
			Password: req.Password,
		}

		if err := u.Validate(); err != nil {
			s.logger.Info("handleRegistration error:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		if err := s.spamStore.add(r.RemoteAddr); err != nil {
			if err == errIPExists {
				s.logger.Info("handleRegistration error:", http.StatusUnprocessableEntity, errResponseSpam)
				s.response(w, r, http.StatusUnprocessableEntity, errResponseSpam)
			} else {
				s.logger.Info("handleRegistration error:", http.StatusUnprocessableEntity, err)
				s.response(w, r, http.StatusUnprocessableEntity, err)
			}
			return
		}

		if err := u.BeforeCreate(); err != nil {
			s.logger.Info("handleRegistration error:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		if err := s.userStore.Repository().IsUniqueName(u.Name); err != nil {
			s.logger.Debug("handleRegistration error:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		if err := s.userStore.Repository().Create(u); err != nil {
			s.logger.Error("handleRegistration error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		if err := s.setCookie(u, w, r); err != nil {
			s.logger.Error("handleAuthorization error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		go telegram.SendMessage("Create user: " + u.Name)

		token, err := token.Create(u.Name, s.config.TokenKey)
		if err != nil {
			s.logger.Error("handleRegistration error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		sessionID, err := getSessionIDCookie(w)
		if sessionID == "" {
			s.logger.Error("handleRegistration error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		w.Header().Add(HeaderSessionID, sessionID)
		w.Header().Add(HeaderUserName, u.Name)
		w.Header().Add(HeaderUserToken, token)
		s.response(w, r, http.StatusCreated, ResponseAuth{u.Name, token, sessionID})
	}
}

type requestAuthorization struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// handleAuthorization ...
func (s *service) handleAuthorization() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := &requestAuthorization{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.logger.Debug("handleAuthorization error:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err)
			return
		}

		u, err := s.userStore.Repository().FindByName(req.Username)
		if err != nil {
			s.logger.Debug("handleAuthorization error:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err)
			return
		}
		if !u.ComparePassword(req.Password) {
			s.logger.Error("handleAuthorization error:", http.StatusUnauthorized, user.ErrIncorrectNameOrPassword)
			s.response(w, r, http.StatusUnauthorized, user.ErrIncorrectNameOrPassword)
			return
		}

		if err := s.setCookie(u, w, r); err != nil {
			s.logger.Error("handleAuthorization error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		token, err := token.Create(u.Name, s.config.TokenKey)
		if err != nil {
			s.logger.Error("handleAuthorization error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		sessionID, err := getSessionIDCookie(w)
		if sessionID == "" {
			s.logger.Error("handleRegistration error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		w.Header().Add(HeaderSessionID, sessionID)
		w.Header().Add(HeaderUserName, u.Name)
		w.Header().Add(HeaderUserToken, token)
		s.response(w, r, http.StatusOK, ResponseAuth{u.Name, token, sessionID})
	}
}

// handleHealthcheck ...
func (s *service) handleHealthcheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data := struct {
			Status string
		}{
			"ok",
		}
		s.response(w, r, http.StatusOK, data)
	}
}

// setCookie ...
func (s *service) setCookie(u *user.User, w http.ResponseWriter, r *http.Request) error {
	session, err := s.sessionStore.Get(r, SessionCookieName)
	if err != nil {
		return err
	}

	session.Values["user_id"] = u.ID
	if err := s.sessionStore.Save(r, w, session); err != nil {
		return err
	}

	return nil
}

// getSessionIDCookie ...
func getSessionIDCookie(w http.ResponseWriter) (string, error) {
	var sessionID string
	var err error

	if cookies := w.Header().Get("Set-Cookie"); cookies != "" {
		// бъем куку на часто по ";"
		split := strings.Split(string(cookies), ";")

		// бъем куку на часто по "="
		split = strings.Split(split[0], "=")

		if len(split) == 2 && split[0] == SessionCookieName {
			sessionID = split[1]
		}
	}

	if sessionID == "" {
		err = errors.New("Cookie session_id not set")
	}

	return sessionID, err
}

// response ...
func (s *service) response(w http.ResponseWriter, r *http.Request, code int, data interface{}) {
	w.WriteHeader(code)
	if err, ok := data.(error); ok {
		data = map[string]string{"error": err.Error()}
	}
	if data != nil {
		json.NewEncoder(w).Encode(data)
	}
	s.logger.Debug("Code:", code, " Header: ", w.Header(), " Data: ", data)
}
