package service

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/nechhist/tt_user/internal/config"
	//"gitlab.com/nechhist/tt_user/internal/model/user/store_db"
	"github.com/gorilla/sessions"
	"gitlab.com/nechhist/tt_user/internal/model/user/store_inmemory"
	log "gitlab.com/nechhist/tt_user/pkg/logger"
)

// Run ...
func Run(cfg *config.Config, logger log.Logger) error {

	// store
	userStore := store_inmemory.New() // If you want to work with the database in memory
	// db, err := store_db.NewMysql(cfg.DatabaseURL)
	// if err != nil {
	// 	return err
	// }
	// defer db.Close()
	// userStore := store_db.New(db)

	// session
	sessionStore := sessions.NewCookieStore([]byte(cfg.SessionKey))
	sessionStore.Options.MaxAge = 86400 * 180
	sessionStore.Options.HttpOnly = true

	// http
	address := fmt.Sprintf(":%v", cfg.ServerPort)
	s := &service{
		httpserver: &http.Server{
			Addr:         address,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 5 * time.Second,
		},
		logger:       logger,
		userStore:    userStore,
		sessionStore: sessionStore,
		config:       cfg,
	}
	s.httpserver.Handler = s.getRouters()

	// spamStore
	s.spamStore = newSpamStore()
	go s.spamStore.runGC()

	// run
	go shutdown(s.httpserver, 2*time.Second, logger)
	logger.Infof("Server is running at %v", address)
	return s.httpserver.ListenAndServe()
}
