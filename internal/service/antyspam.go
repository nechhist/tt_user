package service

import (
	"errors"
	"net"
	"sync"
	"time"
)

var (
	// spamDelay ...
	spamDelay = 1
	// errIPExists ...
	errIPExists = errors.New("Ip Exists")
)

// spamStore ...
type spamStore struct {
	ips map[string]struct{}
	mu  sync.Mutex
}

// newSpamStore ...
func newSpamStore() *spamStore {
	return &spamStore{
		ips: make(map[string]struct{}),
	}
}

// add ...
func (s *spamStore) add(addr string) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	ip, _, _ := net.SplitHostPort(addr)

	if _, ok := s.ips[ip]; ok {
		return errIPExists
	}

	s.ips[ip] = struct{}{}
	return nil
}

// runGC ...
func (s *spamStore) runGC() {
	ticker := time.NewTicker(time.Duration(spamDelay) * time.Minute)
	for range ticker.C {
		go s.clean()
	}
}

// clean ...
func (s *spamStore) clean() {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.ips = make(map[string]struct{})
}
