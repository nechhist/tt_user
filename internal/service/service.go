package service

import (
	"net/http"

	"github.com/gorilla/sessions"
	"gitlab.com/nechhist/tt_user/internal/config"
	"gitlab.com/nechhist/tt_user/internal/model/user"
	log "gitlab.com/nechhist/tt_user/pkg/logger"
)

const (
	// Version app
	Version = "1.0.0"
)

type service struct {
	config       *config.Config
	httpserver   *http.Server
	logger       log.Logger
	userStore    user.Store
	sessionStore sessions.Store
	spamStore    *spamStore
}
