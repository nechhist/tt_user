package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"

	// "github.com/gopherschool/http-rest-api/internal/app/store/teststore"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nechhist/tt_user/internal/config"
	"gitlab.com/nechhist/tt_user/internal/model/user"
	"gitlab.com/nechhist/tt_user/internal/model/user/store_inmemory"
	log "gitlab.com/nechhist/tt_user/pkg/logger"
	"go.uber.org/zap/zapcore"
)

var (
	sessionKeyTest = []byte("secret")
)

func Test_HandleRegistration_Code(t *testing.T) {
	username, password, email := "test", "password", "test@mail.com"
	testUser := getTestUser(t, username, password, email)
	s := getTestServer(t, testUser)

	testCases := []struct {
		nameCase string
		payload  interface{}
		code     int
	}{
		{
			nameCase: "ok",
			payload: map[string]interface{}{
				"username": "username2", // not unique with username
				"email":    email,
				"password": password,
			},
			code: http.StatusCreated,
		},
		{
			nameCase: "check unique",
			payload: map[string]interface{}{
				"username": username,
				"email":    testUser.Email,
				"password": password,
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			nameCase: "check short name",
			payload: map[string]interface{}{
				"username": "x",
				"email":    email,
				"password": password,
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			nameCase: "check short password",
			payload: map[string]interface{}{
				"username": username,
				"email":    testUser.Email,
				"password": "x",
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			nameCase: "invalid payload",
			payload:  "invalid",
			code:     http.StatusBadRequest,
		},
		{
			nameCase: "invalid email",
			payload: map[string]interface{}{
				"username": "qwerty",
				"email":    "invalid",
				"password": "qwertyui",
			},
			code: http.StatusUnprocessableEntity,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.nameCase, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, urlApiV1+urlRegistration, b)
			s.httpserver.Handler.ServeHTTP(rec, req)
			assert.Equal(t, tc.code, rec.Code)
		})
	}
}

func Test_HandleAuthorization_Code(t *testing.T) {
	username, password, email := "test", "password", "test@mail.com"
	testUser := getTestUser(t, username, password, email)
	s := getTestServer(t, testUser)

	testCases := []struct {
		nameCase string
		payload  interface{}
		code     int
	}{
		{
			nameCase: "ok",
			payload: map[string]interface{}{
				"username": username,
				"password": password,
			},
			code: http.StatusOK,
		},
		{
			nameCase: "invalid name",
			payload: map[string]interface{}{
				"username": "invalid",
				"password": password,
			},
			code: http.StatusUnauthorized,
		},
		{
			nameCase: "invalid password",
			payload: map[string]interface{}{
				"username": username,
				"password": "invalid",
			},
			code: http.StatusUnauthorized,
		},
		{
			nameCase: "invalid payload",
			payload:  "invalid",
			code:     http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.nameCase, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, urlApiV1+urlAuthorization, b)
			s.httpserver.Handler.ServeHTTP(rec, req)

			assert.Equal(t, tc.code, rec.Code)
		})
	}
}

func Test_HandleIdentification_Response(t *testing.T) {
	username, password, email := "test", "password", "test@mail.com"
	testUser := getTestUser(t, username, password, email)
	s := getTestServer(t, testUser)

	testCases := []struct {
		name         string
		cookieValue  map[interface{}]interface{}
		expectedCode int
	}{
		{
			name: "authenticated /api/v1/user/id",
			cookieValue: map[interface{}]interface{}{
				"user_id": testUser.ID,
			},
			expectedCode: http.StatusOK,
		},
		{
			name:         "not authenticated /api/v1/user/id",
			cookieValue:  nil,
			expectedCode: 401,
		},
	}

	sc := securecookie.New(sessionKeyTest, nil)

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, urlApiV1+urlUser+urlIdentification, nil)
			cookieStr, _ := sc.Encode(SessionCookieName, tc.cookieValue)
			req.Header.Set("Cookie", fmt.Sprintf("%s=%s", SessionCookieName, cookieStr))
			s.httpserver.Handler.ServeHTTP(rec, req)
			assert.Equal(t, tc.expectedCode, rec.Code)

			if rec.Code >= 400 {
				return
			}

			// check body
			response := &ResponseAuth{}
			if err := json.NewDecoder(rec.Body).Decode(response); err != nil {
				t.Error(err)
			}
			if response.Name != username {
				t.Errorf("esponse.Name != username, response.Name: %v, username: %v", response.Name, username)
			}
			if response.Token == "" {
				t.Errorf("response.Token is empty")
			}
		})
	}
}

func Test_HandleAuthorization_Response(t *testing.T) {
	username, password, email := "test", "password", "test@mail.com"
	testUser := getTestUser(t, username, password, email)
	s := getTestServer(t, testUser)

	// get response
	payload := requestAuthorization{
		Username: username,
		Password: password,
	}
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(payload)
	res := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, urlApiV1+urlAuthorization, b)
	s.httpserver.Handler.ServeHTTP(res, req)

	// sessionId get
	_, err := GetResponseRecorderCookie(res)
	if err != nil {
		t.Error(err)
	}

	// check token and name
	fmt.Println("Body:", res.Body)

	response := &ResponseAuth{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		t.Error(err)
	}

	if response.Name != username {
		t.Errorf("esponse.Name != username, response.Name: %v, username: %v", response.Name, username)
	}
	if response.Token == "" {
		t.Errorf("response.Token is empty")
	}

	assert.Equal(t, http.StatusOK, res.Code)
}

func Test_HandleRegistration_Response(t *testing.T) {
	username, password, email := "test", "password", "test@mail.com"
	s := getTestServer(t, nil)

	// get response
	payload := requestRegistration{
		Username: username,
		Email:    email,
		Password: password,
	}
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(payload)
	res := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, urlApiV1+urlRegistration, b)
	s.httpserver.Handler.ServeHTTP(res, req)

	// sessionId get
	_, err := GetResponseRecorderCookie(res)
	if err != nil {
		t.Error(err)
	}

	// check token and name
	fmt.Println("Body:", res.Body)

	response := &ResponseAuth{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		t.Error(err)
	}

	if response.Name != username {
		t.Errorf("esponse.Name != username, response.Name: %v, username: %v", response.Name, username)
	}
	if response.Token == "" {
		t.Errorf("response.Token is empty")
	}

	assert.Equal(t, http.StatusCreated, res.Code)
}

func GetResponseRecorderCookie(res *httptest.ResponseRecorder) (string, error) {
	cookies, ok := res.HeaderMap["Set-Cookie"]
	if !ok {
		return "", errors.New("ResponseRecorder have not Set-Cookie")
	}

	session_id := ""
	for _, str := range cookies {
		// бъем куку на часто по ";"
		split := strings.Split(string(str), ";")
		// бъем куку на часто по "="
		split = strings.Split(split[0], "=")
		if len(split) == 2 && split[0] == SessionCookieName {
			session_id = split[1]
		}
	}
	if session_id == "" {
		return "", errors.New("ResponseRecorder Cookie session_id not found")
	}
	return session_id, nil
}

// GetTestUser ...
func getTestUser(t *testing.T, name, password, email string) *user.User {
	t.Helper()

	return &user.User{
		Name:     name,
		Email:    email,
		Password: password,
	}
}

// getTestServer
func getTestServer(t *testing.T, testUser *user.User) *service {
	t.Helper()

	userStore := store_inmemory.New()
	if testUser != nil {
		testUser.BeforeCreate()
		userStore.Repository().Create(testUser)
	}

	sessionStore := sessions.NewCookieStore(sessionKeyTest)
	s := &service{
		httpserver:   &http.Server{},
		logger:       log.New(zapcore.InfoLevel),
		userStore:    userStore,
		sessionStore: sessionStore,
	}
	s.config = &config.Config{TokenKey: "test", AllowedOrigins: []string{"*"}}
	s.httpserver.Handler = s.getRouters()
	s.spamStore = newSpamStore()

	return s
}
