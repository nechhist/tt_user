package service

import (
	"context"
	"fmt"
	"net/http"
	"runtime/debug"
	"time"
)

const ctxUser int8 = 0

// responseWriter
type responseWriter struct {
	http.ResponseWriter
	code int
}

// errorMiddleware
func (s *service) errorMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			l := s.logger.With(r.Context())
			if e := recover(); e != nil {
				var ok bool
				var err error
				if err, ok = e.(error); !ok {
					err = fmt.Errorf("%v", e)
				}
				if err != nil {
					s.response(w, r, http.StatusInternalServerError, err)
				}
				l.Errorf("recovered from panic (%v): %s", err, debug.Stack())
			}
		}()
		next.ServeHTTP(w, r)
	})
}

// accessMiddleware
func (s *service) accessMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		rw := &responseWriter{w, http.StatusOK}
		next.ServeHTTP(rw, r)

		s.logger.Infof(
			"Access Log: %s %s %s %d %dms",
			r.Method,
			r.URL.Path,
			r.Proto,
			rw.code,
			time.Since(start).Milliseconds(),
		)
	})
}

// authenticationMiddleware
func (s *service) authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := s.sessionStore.Get(r, SessionCookieName)
		if err != nil {
			s.logger.Debug("Auth log (1) code: %v, %s", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err)
			return
		}

		id, ok := session.Values["user_id"]
		if !ok {
			s.logger.Debug("Auth log (2) user_id %s, code: %v, %s", id, http.StatusUnauthorized, ErrNotAuthenticated)
			s.response(w, r, http.StatusUnauthorized, ErrNotAuthenticated)
			return
		}

		u, err := s.userStore.Repository().FindById(id.(int))
		if err != nil {
			s.logger.Debug("Auth log (3) user_id: %s, code: %v, %s", id, http.StatusUnauthorized, ErrNotAuthenticated)
			s.response(w, r, http.StatusUnauthorized, ErrNotAuthenticated)
			return
		}
		//s.response(w, r, http.StatusOK, u)
		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), ctxUser, u)))
	})
}
