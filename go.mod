module gitlab.com/nechhist/tt_user

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496 // indirect
	github.com/go-ozzo/ozzo-dbx v1.5.0
	github.com/go-ozzo/ozzo-routing/v2 v2.3.0
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-ozzo/ozzo-validation/v3 v3.8.1
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/gopherschool/http-rest-api v0.0.0-20190922093049-d59926c5ce3c // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/qiangxue/go-env v1.0.0
	github.com/qiangxue/go-restful-api v1.3.1
	github.com/stretchr/testify v1.4.0
	gitlab.com/nechhist/laby v0.0.0-20200116170904-07af9bc437c7
	go.uber.org/zap v1.13.0
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
	gopkg.in/yaml.v2 v2.2.7
)
