CREATE TABLE users (
  id int(11) not null primary key,
  name varchar(16) not null unique,
  email varchar(255) not null,
  encrypted_password varchar(255) not null
  create_at int(11) not null
);
