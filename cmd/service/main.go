package main

import (
	"os"

	"gitlab.com/nechhist/tt_user/internal/config"
	"gitlab.com/nechhist/tt_user/internal/service"
	logger "gitlab.com/nechhist/tt_user/pkg/logger"
)

// Version indicates the current version of the application.
var Version = "0.9.0"

func main() {
	// config
	cfg := config.Load()

	// logger
	logLevel := logger.GetLevel(cfg.LogLevel)
	log := logger.New(logLevel)

	if err := cfg.Validate(); err != nil {
		log.Error(err)
		os.Exit(-1)
	}
	// run
	log.Error(service.Run(cfg, log))
}
